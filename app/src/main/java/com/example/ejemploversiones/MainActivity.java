package com.example.ejemploversiones;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etPhone = (EditText) findViewById(R.id.editTextPhone);
        EditText etext = (EditText) findViewById(R.id.editTextName);

        Button btn = (Button) findViewById(R.id.button1);
        Button btn2 = (Button) findViewById(R.id.button2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = etPhone.getText().toString();

                if (num != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        versionNueva();
                    } else {
                        versionesAnteriores(num);
                    }
                }
            }


            private void versionesAnteriores(String num) {
                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)) {
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "No tienes permiso", Toast.LENGTH_SHORT);
                }
            }

            private void versionNueva() {
                //
            }
        });

    }


    private boolean verificarPermisos(String permiso) {
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}